/*
 * Arduino_Beehive_Scale.cpp
 *
 *  Created on: Oct 3, 2014
 *      Author: spelle
 */



#include "SoftwareSerial.h"
#include <EEPROM.h>

#include <avr/wdt.h>
#include <util/crc16.h>

#include "Arduino_Beehive_Scale.h"
#include "Arduino_Beehive_Scale_Data.h"

#include "beehiveFieldProtocol.h"

#include <utility/util.h>



#define UPDATE_GAUGES_PERIOD	4999
#define BLINK_LED_PERIOD		999
#define SERIAL_WATCHDOG_PERIOD	999

#ifdef SCALE_SIMU
		#define SIMULATIONSINE_PERIOD	999
#endif



typedef enum
{	initState,
	dataFrameReceive,
	dataFrameComplete,
	requestValid,
	responseBuild,
	responseSend,
	responseSent,
} requestReceiveStateMachine_t ;



SoftwareSerial SWSerial(10, 11) ; // RX, TX
#define TX_ENABLE_PIN 9



#define unitd_EEPROM_ADDRESS 0
unitId_EEPROM_t unitId_EEPROM =
{
	0x12345678
};



void setup()
{
	// Set PB5 as OUTPUT
	DDRB |= (1 << PB5 ) ;

	// Initialize SWSerial Link
	SWSerial.begin( FIELD_LINK_BAUD_RATE ) ;

	pinMode( TX_ENABLE_PIN, OUTPUT ) ;
	digitalWrite( TX_ENABLE_PIN, LOW ) ;

	// Read unitId from EEPROM
	unitId_EEPROM_t unitId ;
	EEPROM.get( unitd_EEPROM_ADDRESS, unitId) ;
	if( unitId.unitId !=  unitId_EEPROM.unitId )
		EEPROM.put( unitd_EEPROM_ADDRESS, unitId_EEPROM ) ;

	EEPROM.get( unitd_EEPROM_ADDRESS, unitId_EEPROM) ;

	// Read from EEPROM
	EEPROM.get( 0, unitId_EEPROM) ;

	DEBUG_INIT_SERIAL ;

	PDEBUG( "--- Arduino Beehive Scale ---\n" ) ;
	PDEBUG( "- unit ID : 0x%08lX\n", unitId_EEPROM.unitId ) ;
}

void loop()
{
	static unsigned long	ulCurrentTime			= 0 ;

	static unsigned long	ulNextSerialWatchdog	= SERIAL_WATCHDOG_PERIOD ;
	static boolean			serialWatchdogKicked	= false ;

	static unsigned long	ulNextTimeGauges		= UPDATE_GAUGES_PERIOD ;
	static unsigned long	ulNextTimeLED			= BLINK_LED_PERIOD ;

	static requestReceiveStateMachine_t requestReceiveStateMachine = initState ;

	static byte					rcvdBeehiveDataFrameBuffer[MAX(sizeof(beehiveReadDataRequest_t),sizeof(beehiveReadDataResponse_t))] ;
	static uint8_t				rcvdBeehiveDataFrameCptr		= 0 ;

	static beehiveReadDataRequest_t * pBeehiveReadDataRequest	= (beehiveReadDataRequest_t *) rcvdBeehiveDataFrameBuffer ;

	static beehiveReadDataResponse_t beehiveReadDataResponse ;
	static byte *			pBeehiveResponse					= (byte *) &beehiveReadDataResponse ;
	static uint8_t			sendBeehiveDataReadResponseCptr		= 0 ;

	static value_t			gaugeNo1Value = 0 ;
	static value_t			gaugeNo1RawValue = 0 ;
	static value_t			gaugeNo2Value = 0 ;
	static value_t			gaugeNo2RawValue = 0 ;
	static value_t			gaugeNo3Value = 0 ;
	static value_t			gaugeNo3RawValue = 0 ;
	static value_t			scaleValue = 0 ;
	static value_t			scaleRawValue = 0 ;

	uint16_t CRC = 0 ;

	DEBUG_ROTATE ;

	ulCurrentTime = millis() ;

	// process the DataReadRequests in priority

	switch( requestReceiveStateMachine )
	{
		case initState :

			PDEBUG( "- requestReceiveStateMachine : initState\n" );

			memset( (void *) rcvdBeehiveDataFrameBuffer, 0, sizeof(rcvdBeehiveDataFrameBuffer) ) ;

			digitalWrite( TX_ENABLE_PIN, LOW ) ;

			PDEBUG( "- requestReceiveStateMachine : dataFrameReceive\n" );

			// initialize Counters
			rcvdBeehiveDataFrameCptr = 0 ;
			sendBeehiveDataReadResponseCptr = 0 ;

			requestReceiveStateMachine = dataFrameReceive ;

			break ;

		case dataFrameReceive :

			// Read the SWSerial
			if( SWSerial.available() )
			{
				// Kick the watchdog
				serialWatchdogKicked = true ;

				rcvdBeehiveDataFrameBuffer[rcvdBeehiveDataFrameCptr] = SWSerial.read() ;

				// PDEBUG( "%02X\n", (uint8_t) rcvdBeehiveDataFrameBuffer[rcvdBeehiveDataFrameCptr] ) ;
				rcvdBeehiveDataFrameCptr ++ ;

				if( rcvdBeehiveDataFrameCptr >= MIN(sizeof(beehiveReadDataRequest_t),sizeof(beehiveReadDataResponse_t)) )
				{	// we may have a complete message...
					if( ( pBeehiveReadDataRequest->serviceId == beehiveDataReadRequest &&
						  rcvdBeehiveDataFrameCptr == sizeof(beehiveReadDataRequest_t) ) ||
						( pBeehiveReadDataRequest->serviceId == beehiveDataReadResponse &&
							 rcvdBeehiveDataFrameCptr == sizeof(beehiveReadDataResponse_t) ) )
					{
						// This is a ReadDataRequest
						TRACE( (const char *)pBeehiveReadDataRequest, pBeehiveReadDataRequest->frameSize ) ;

						requestReceiveStateMachine = dataFrameComplete ;
					}
				}
			}

			break ;

		case dataFrameComplete :

			PDEBUG( "\n- requestReceiveStateMachine : dataFrameComplete\n" );

			// hopefully, this is a request for us
			pBeehiveReadDataRequest = (beehiveReadDataRequest_t *) rcvdBeehiveDataFrameBuffer ;

			// Calculate the CRC
			CRC = 0xffff ;

			for( uint8_t i = 0 ; i < (pBeehiveReadDataRequest->frameSize - sizeof( pBeehiveReadDataRequest->CRC)) ; i ++ )
				CRC = _crc_ccitt_update( CRC, rcvdBeehiveDataFrameBuffer[i] ) ;

			if(	( pBeehiveReadDataRequest->serviceId == beehiveDataReadRequest ) &&
				( pBeehiveReadDataRequest->beehiveId == htonl(unitId_EEPROM.unitId )  ) &&
				( pBeehiveReadDataRequest->CRC       == htons(CRC) ) )
			{
				requestReceiveStateMachine = requestValid ;
			}
			else
			{
				requestReceiveStateMachine = initState ;
			}

			break ;

		case requestValid :

			// Request is valid. It shall be answered.
			PDEBUG( "- requestReceiveStateMachine : requestValid\n" );
			requestReceiveStateMachine = responseBuild ;

			break ;

		case responseBuild :

			PDEBUG( "- requestReceiveStateMachine : responseBuild\n" );

			// Build the Request
			beehiveReadDataResponse.frameSize = sizeof(beehiveReadDataResponse) ;
			beehiveReadDataResponse.serviceId = beehiveDataReadResponse ;
			beehiveReadDataResponse.beehiveId = htonl(unitId_EEPROM.unitId) ;
			beehiveReadDataResponse.gaugeNo1Value = htons(gaugeNo1Value) ;
			beehiveReadDataResponse.gaugeNo1RawValue = htons(gaugeNo1RawValue) ;
			beehiveReadDataResponse.gaugeNo2Value = htons(gaugeNo2Value) ;
			beehiveReadDataResponse.gaugeNo2RawValue = htons(gaugeNo2RawValue) ;
			beehiveReadDataResponse.gaugeNo3Value = htons(gaugeNo3Value) ;
			beehiveReadDataResponse.gaugeNo3RawValue = htons(gaugeNo3RawValue) ;
			beehiveReadDataResponse.scaleValue = htons(scaleValue) ;
			beehiveReadDataResponse.scaleRawValue = htons(scaleRawValue) ;

			// Calculate the CRC
			CRC = 0xffff ;

			for( uint8_t i = 0 ; i < (beehiveReadDataResponse.frameSize - sizeof( beehiveReadDataResponse.CRC)) ; i ++ )
				CRC = _crc_ccitt_update( CRC, pBeehiveResponse[i] ) ;

			beehiveReadDataResponse.CRC = htons(CRC) ;

			TRACE( (const char *)&beehiveReadDataResponse, beehiveReadDataResponse.frameSize ) ;

			PDEBUG( "- requestReceiveStateMachine : responseSend\n" );

			requestReceiveStateMachine = responseSend ;
			digitalWrite( TX_ENABLE_PIN, HIGH ) ;

			break ;

		case responseSend :

			//PDEBUG( "%02X", (uint8_t) pBeehiveResponse[sendBeehiveDataReadResponseCptr] ) ;

			SWSerial.write( pBeehiveResponse[sendBeehiveDataReadResponseCptr] ) ;
			sendBeehiveDataReadResponseCptr ++ ;

			if( sizeof(beehiveReadDataResponse) <= sendBeehiveDataReadResponseCptr )
			{	// At end of "buffer"
				requestReceiveStateMachine = responseSent ;
			}

			break ;

		case responseSent :

			PDEBUG( "\n- requestReceiveStateMachine : responseSent\n" );

			digitalWrite( TX_ENABLE_PIN, LOW ) ;

			// Nothing more to do. return to init state
			requestReceiveStateMachine = initState ;
			break ;
	}

	if( ulCurrentTime > ulNextSerialWatchdog )
	{
		// has the watchdog been kicked ?
		if( true == serialWatchdogKicked )
		{	// yes, it has. Simply reset its state
			serialWatchdogKicked = false ;
		}
		else
		{	// No, it hasn't. this means no data has been received for SERIAL_WATCHDOG_PERIOD ms
			// Reset the requestReceiveStateMachine
			requestReceiveStateMachine = initState ;
			// Kick the watchdog
			serialWatchdogKicked = true ;
		}

		ulNextSerialWatchdog = ulCurrentTime + SERIAL_WATCHDOG_PERIOD ;
	}

	if( ulCurrentTime > ulNextTimeGauges )
	{
		PDEBUG( "%lu : Reading Gauges\n", ulCurrentTime ) ;
#ifndef SCALE_SIMU
		// Update gauges values
		gaugeNo1RawValue = analogRead(0) ;
		gaugeNo2RawValue = analogRead(1) ;
		gaugeNo3RawValue = analogRead(2) ;
#else
		gaugeNo1RawValue = getSineValue() ;
		gaugeNo2RawValue = getSineValue() ;
		gaugeNo3RawValue = getSineValue() ;
#endif
		gaugeNo1Value = gaugeNo1RawValue * 25 / 1024 ;
		gaugeNo2Value = gaugeNo2RawValue * 25 / 1024 ;
		gaugeNo3Value = gaugeNo3RawValue * 25 / 1024 ;

		scaleRawValue = gaugeNo1RawValue + gaugeNo2RawValue + gaugeNo3RawValue ;
		scaleValue = scaleRawValue * 25 / 1024 ;

		PDEBUG( "%lu : Gauge #1 Raw : %u\n", ulCurrentTime, gaugeNo1RawValue ) ;
		PDEBUG( "%lu : Gauge #1 : %u\n", ulCurrentTime, gaugeNo1Value ) ;
		PDEBUG( "%lu : Gauge #2 Raw : %u\n", ulCurrentTime, gaugeNo2RawValue ) ;
		PDEBUG( "%lu : Gauge #2 : %u\n", ulCurrentTime, gaugeNo2Value ) ;
		PDEBUG( "%lu : Gauge #3 Raw : %u\n", ulCurrentTime, gaugeNo3RawValue ) ;
		PDEBUG( "%lu : Gauge #3 : %u\n", ulCurrentTime, gaugeNo3Value ) ;

		PDEBUG( "%lu : Scale Raw Value : %u\n", ulCurrentTime, scaleValue ) ;
		PDEBUG( "%lu : Scale Raw Value : %u\n", ulCurrentTime, scaleRawValue ) ;

		ulNextTimeGauges = ulCurrentTime + UPDATE_GAUGES_PERIOD ;
	}


	if( ulCurrentTime > ulNextTimeLED )
	{
		PORTB ^= (1 << PB5 ) ;

		ulNextTimeLED = ulCurrentTime + BLINK_LED_PERIOD ;
	}
}




