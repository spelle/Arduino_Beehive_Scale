/*
 * Arduino_Beehive_Scale.h
 *
 *  Created on: Oct 4, 2014
 *      Author: nuptse
 */

#ifndef ARDUINO_BEEHIVE_SCALE_H_
#define ARDUINO_BEEHIVE_SCALE_H_

#include <Arduino.h>
#include <Arduino_Beehive_Unit_EEPROM.h>



#define MAX(a, b) ((a) < (b) ? (b) : (a))
#define MIN(a, b) ((a) > (b) ? (b) : (a))



#define SCALE_SIMU

#ifdef SCALE_SIMU
	#include "Arduino_Simu_Sine.h"
#endif // SCALE_SIMU



#define DEBUG



#ifdef DEBUG
	#include "Arduino_PDEBUG.h"
#endif // DEBUG

#endif /* ARDUINO_BEEHIVE_SCALE_H_ */
