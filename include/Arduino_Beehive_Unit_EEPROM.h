/*
 * Beehive_Unit_EEPROM.h
 *
 *  Created on: Oct 4, 2014
 *      Author: nuptse
 */

#ifndef BEEHIVE_UNIT_EEPROM_H_
#define BEEHIVE_UNIT_EEPROM_H_

#include "beehiveFieldProtocol.h"

typedef struct
{
	unitId_t	unitId ;
} unitId_EEPROM_t ;



#endif /* BEEHIVE_UNIT_EEPROM_H_ */
