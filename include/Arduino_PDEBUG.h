/*
 * Arduino_PDEBUG.h
 *
 *  Created on: Oct 1, 2015
 *      Author: sherpa
 */

#ifndef INCLUDE_ARDUINO_PDEBUG_H_
#define INCLUDE_ARDUINO_PDEBUG_H_

#ifdef DEBUG

#define _PDEBUG

#define DEBUG_ROTATE \
	static char debugRotate[] = "-/|\\" ; \
	static uint8_t uiDebugRotate = 0 ; \
\
	PDEBUG( "%c", debugRotate[uiDebugRotate++%4] ) ; \
	PDEBUG( "\033[1D" ) ; \
\
	static char aChar = 0 ; \
\
	if( Serial.available() ) \
	{ \
		Serial.readBytes( &aChar, sizeof(aChar)) ; \
\
		if ( 'p' == aChar ) \
		{ \
			PDEBUG( "\nExecution paused...\nType 'c' to continue.\n" ) ; \
			do \
			{ \
				Serial.readBytes( &aChar, sizeof(aChar)) ;\
			} \
			while ( 'c' != aChar ) ; \
			PDEBUG( "Execution will continue.\n\n" ) ; \
		} \
	} \

#define DEBUG_INIT_SERIAL \
	Serial.begin(115200); \
	/*while (!Serial) ; */\
\
	stdout = & m_stdout ; \


#else

#define DEBUG_ROTATE
#define DEBUG_INIT_SERIAL

#endif // DEBUG


#ifdef _PDEBUG
#include <stdio.h>


int Serial_putchar_printf( char var, FILE *stream )
{
	if ( var == '\n' )
	{
		Serial.print( (char) '\r') ;
	}

	Serial.print( (char) var ) ;

	return 0 ;
}



void trace( const char *s, size_t len )
{
	printf( "* TRACE : 0x%02X : ", len ) ;

	for( size_t i =0 ; i < len ; i++ )
	{
		printf( "%02X", (uint8_t) s[i] ) ;

		if( i % 4 == 3 )
			printf( " " ) ;
	}

	printf( "\n" ) ;
}

#undef FDEV_SETUP_STREAM
#define FDEV_SETUP_STREAM(p, g, f) { 0, 0, f, 0, 0, p, g, 0 }
static FILE m_stdout = FDEV_SETUP_STREAM( Serial_putchar_printf, NULL, _FDEV_SETUP_WRITE ) ;

#define PDEBUG(FMT, ...) \
		printf( FMT, ##__VA_ARGS__)
#define TRACE( S, L ) \
		trace( S, L )
#else
	/* not debugging: nothing, it's a placeholder */
	#define PDEBUG(FMT, ...) \
		//
	#define TRACE( S, L ) \
		//
#endif // _PDEBUG



#endif /* INCLUDE_ARDUINO_PDEBUG_H_ */
